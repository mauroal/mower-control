package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.InitializationConfiguration;
import fr.ippon.blablacar.mowercontrol.model.Lawn;
import fr.ippon.blablacar.mowercontrol.model.Mower;
import fr.ippon.blablacar.mowercontrol.model.Orientation;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class InitializationServiceTest {

    private InitializationService initializationService;

    @Test
    public void extractLawn_shouldReturnLawnFromLawnLine() {
        // Given
        initializationService = new InitializationService();

        // When
        Lawn lawn = initializationService.extractLawn("10 20");

        // Then
        assertThat(lawn.getWidth()).isEqualTo(10);
        assertThat(lawn.getHeight()).isEqualTo(20);
    }

    @Test
    public void extractLawn_shouldThrowError_whenLawnLineIsNull() {
        // Given
        initializationService = new InitializationService();

        // When
        Throwable throwable = catchThrowable(() -> initializationService.extractLawn(null));

        // Then
        assertThat(throwable).isInstanceOf(RuntimeException.class).hasMessage("Lawn configuration should be like '5 5' at the first line of 'input.txt' file");
    }

    @Test
    public void extractLawn_shouldThrowError_whenLawnLineIsBlank() {
        // Given
        initializationService = new InitializationService();

        // When
        Throwable throwable = catchThrowable(() -> initializationService.extractLawn("     "));

        // Then
        assertThat(throwable).isInstanceOf(RuntimeException.class).hasMessage("Lawn configuration should be like '5 5' at the first line of 'input.txt' file");
    }

    @Test
    public void extractLawn_shouldThrowError_whenLawnLineIsIncomplete() {
        // Given
        initializationService = new InitializationService();

        // When
        Throwable throwable = catchThrowable(() -> initializationService.extractLawn("5    "));

        // Then
        assertThat(throwable).isInstanceOf(RuntimeException.class).hasMessage("Lawn configuration should be like '5 5' at the first line of 'input.txt' file");
    }

    @Test
    public void extractMower_shouldReturnMowerFromMowerLine() {
        // Given
        initializationService = new InitializationService();

        // When
        Mower mower = initializationService.extractMower("1 1 W", 1L);

        // Then
        assertThat(mower.getId()).isEqualTo(1L);
        assertThat(mower.getPosition().getX()).isEqualTo(1);
        assertThat(mower.getPosition().getY()).isEqualTo(1);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.W);
    }

    @Test
    public void extractMower_shouldThrowError_whenMowerLineIsNull() {
        // Given
        initializationService = new InitializationService();

        // When
        Throwable throwable = catchThrowable(() -> initializationService.extractMower(null, 1L));

        // Then
        assertThat(throwable).isInstanceOf(RuntimeException.class).hasMessage("Mower configuration should be like '1 2 N'");
    }

    @Test
    public void extractMower_shouldThrowError_whenMowerLineIsBlank() {
        // Given
        initializationService = new InitializationService();

        // When
        Throwable throwable = catchThrowable(() -> initializationService.extractMower("       ", 1L));

        // Then
        assertThat(throwable).isInstanceOf(RuntimeException.class).hasMessage("Mower configuration should be like '1 2 N'");
    }

    @Test
    public void extractMower_shouldThrowError_whenMowerLineIsIncomplete() {
        // Given
        initializationService = new InitializationService();

        // When
        Throwable throwable = catchThrowable(() -> initializationService.extractMower("1 1    ", 1L));

        // Then
        assertThat(throwable).isInstanceOf(RuntimeException.class).hasMessage("Mower configuration should be like '1 2 N'");
    }

    @Test
    public void extractConfiguration() {
        // Given
        initializationService = new InitializationService();

        // When
        InitializationConfiguration initializationConfiguration = initializationService.extractConfiguration();

        // Then
        assertThat(initializationConfiguration.getLawn().getWidth()).isEqualTo(5);
        assertThat(initializationConfiguration.getLawn().getHeight()).isEqualTo(5);
        assertThat(initializationConfiguration.getMowerList()).hasSize(2);
        assertThat(initializationConfiguration.getMowerCommandsList()).hasSize(2);
    }

}
