package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.MowerCommands;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CommanderServiceTest {

    @Test
    public void process_shouldTryToMoveMowerForEachCommand() {
        // Given
        LawnService lawnService = Mockito.mock(LawnService.class);
        CommanderService commanderService = new CommanderService(lawnService);
        List<MowerCommands> mowerCommandsList = Arrays.asList(new MowerCommands(1L, "FLFRF"), new MowerCommands(2L, "LFRF"));

        // When
        commanderService.process(mowerCommandsList);

        // Then
        verify(lawnService, times(9)).moveMower(anyLong(), anyString());
    }

}
