package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.Mower;
import fr.ippon.blablacar.mowercontrol.model.Orientation;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MowerServiceTest {

    private MowerService mowerService;

    @Test
    public void getById_shouldReturnMowerById() {
        // Given
        List<Mower> existingMowerList = new ArrayList<>();
        existingMowerList.add(new Mower(1L, 1, 1, "N"));
        existingMowerList.add(new Mower(2L, 2, 2, "E"));
        existingMowerList.add(new Mower(3L, 3, 3, "S"));
        existingMowerList.add(new Mower(4L, 4, 4, "W"));
        mowerService = new MowerService(existingMowerList);

        // When
        Mower mower = mowerService.getById(2L);

        // Then
        assertThat(mower.getId()).isEqualTo(2L);
        assertThat(mower.getPosition().getX()).isEqualTo(2);
        assertThat(mower.getPosition().getY()).isEqualTo(2);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.E);
    }

    @Test
    public void getById_shouldReturnNull_whenRequestedIdDoesNotExist() {
        // Given
        List<Mower> existingMowerList = new ArrayList<>();
        existingMowerList.add(new Mower(1L, 1, 1, "N"));
        existingMowerList.add(new Mower(2L, 2, 2, "E"));
        existingMowerList.add(new Mower(3L, 3, 3, "S"));
        existingMowerList.add(new Mower(4L, 4, 4, "W"));
        mowerService = new MowerService(existingMowerList);

        // When
        Mower mower = mowerService.getById(5L);

        // Then
        assertThat(mower).isNull();
    }

    @Test
    public void getAll_shouldReturnAllMowers() {
        // Given
        List<Mower> existingMowerList = new ArrayList<>();
        existingMowerList.add(new Mower(1L, 1, 1, "N"));
        existingMowerList.add(new Mower(2L, 2, 2, "E"));
        existingMowerList.add(new Mower(3L, 3, 3, "S"));
        existingMowerList.add(new Mower(4L, 4, 4, "W"));
        mowerService = new MowerService(existingMowerList);

        // When
        List<Mower> mowerList = mowerService.getAll();

        assertThat(mowerList).hasSize(4);
    }

}
