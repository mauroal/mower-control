package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.Lawn;
import fr.ippon.blablacar.mowercontrol.model.Mower;
import fr.ippon.blablacar.mowercontrol.model.Orientation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

public class LawnServiceTest {

    private LawnService lawnService;
    private MowerService mowerService;
    private PositionService positionService;
    private Lawn lawn;
    private List<Mower> mowerList;

    @BeforeEach
    void setUp() {
        mowerService = Mockito.mock(MowerService.class);
        positionService = new PositionService();
        lawn = new Lawn(10, 10);
        lawnService = new LawnService(mowerService, positionService, lawn);
        mowerList = new ArrayList<>();
        mowerList.add(new Mower(2L, 2, 2, "E"));
        mowerList.add(new Mower(3L, 3, 3, "S"));
        mowerList.add(new Mower(4L, 4, 4, "W"));
    }

    @Test
    public void moveMower_shouldUpdateMowerPosition_whenNewPositionIsValid() {
        // Given
        Mower mower = new Mower(1L, 1, 1, "N");
        when(mowerService.getById(anyLong())).thenReturn(mower);
        mowerList.add(mower);
        when(mowerService.getAll()).thenReturn(mowerList);

        // When
        lawnService.moveMower(1L, "F");

        // Then
        assertThat(mower.getPosition().getX()).isEqualTo(1);
        assertThat(mower.getPosition().getY()).isEqualTo(2);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.N);
    }

    @Test
    public void moveMower_shouldKeepMowerPosition_whenNewPositionIsSameAsAnotherMower() {
        // Given
        Mower mower = new Mower(1L, 2, 1, "N");
        when(mowerService.getById(anyLong())).thenReturn(mower);
        mowerList.add(mower);
        when(mowerService.getAll()).thenReturn(mowerList);

        // When
        lawnService.moveMower(1L, "F");

        // Then
        assertThat(mower.getPosition().getX()).isEqualTo(2);
        assertThat(mower.getPosition().getY()).isEqualTo(1);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.N);
    }

    @Test
    public void moveMower_shouldKeepMowerPosition_whenNewPositionIsGreaterThanLawnWidth() {
        // Given
        Mower mower = new Mower(1L, 10, 10, "E");
        when(mowerService.getById(anyLong())).thenReturn(mower);
        mowerList.add(mower);
        when(mowerService.getAll()).thenReturn(mowerList);

        // When
        lawnService.moveMower(1L, "F");

        // Then
        assertThat(mower.getPosition().getX()).isEqualTo(10);
        assertThat(mower.getPosition().getY()).isEqualTo(10);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.E);
    }

    @Test
    public void moveMower_shouldKeepMowerPosition_whenNewPositionIsGreaterThanLawnHeight() {
        // Given
        Mower mower = new Mower(1L, 10, 10, "N");
        when(mowerService.getById(anyLong())).thenReturn(mower);
        mowerList.add(mower);
        when(mowerService.getAll()).thenReturn(mowerList);

        // When
        lawnService.moveMower(1L, "F");

        // Then
        assertThat(mower.getPosition().getX()).isEqualTo(10);
        assertThat(mower.getPosition().getY()).isEqualTo(10);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.N);
    }

    @Test
    public void moveMower_shouldKeepMowerPosition_whenNewPositionHasNegativeXPosition() {
        // Given
        Mower mower = new Mower(1L, 0, 0, "W");
        when(mowerService.getById(anyLong())).thenReturn(mower);
        mowerList.add(mower);
        when(mowerService.getAll()).thenReturn(mowerList);

        // When
        lawnService.moveMower(1L, "F");

        // Then
        assertThat(mower.getPosition().getX()).isEqualTo(0);
        assertThat(mower.getPosition().getY()).isEqualTo(0);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.W);
    }

    @Test
    public void moveMower_shouldKeepMowerPosition_whenNewPositionHasNegativeYPosition() {
        // Given
        Mower mower = new Mower(1L, 0, 0, "S");
        when(mowerService.getById(anyLong())).thenReturn(mower);
        mowerList.add(mower);
        when(mowerService.getAll()).thenReturn(mowerList);

        // When
        lawnService.moveMower(1L, "F");

        // Then
        assertThat(mower.getPosition().getX()).isEqualTo(0);
        assertThat(mower.getPosition().getY()).isEqualTo(0);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.S);
    }

    @Test
    public void moveMower_shouldRotateLeft_whenCommandIsL() {
        // Given
        Mower mower = new Mower(1L, 0, 0, "N");
        when(mowerService.getById(anyLong())).thenReturn(mower);
        mowerList.add(mower);
        when(mowerService.getAll()).thenReturn(mowerList);

        // When
        lawnService.moveMower(1L, "L");

        // Then
        assertThat(mower.getPosition().getX()).isEqualTo(0);
        assertThat(mower.getPosition().getY()).isEqualTo(0);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.W);
    }

    @Test
    public void moveMower_shouldRotateRight_whenCommandIsR() {
        // Given
        Mower mower = new Mower(1L, 0, 0, "N");
        when(mowerService.getById(anyLong())).thenReturn(mower);
        mowerList.add(mower);
        when(mowerService.getAll()).thenReturn(mowerList);

        // When
        lawnService.moveMower(1L, "R");

        // Then
        assertThat(mower.getPosition().getX()).isEqualTo(0);
        assertThat(mower.getPosition().getY()).isEqualTo(0);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(Orientation.E);
    }

}
