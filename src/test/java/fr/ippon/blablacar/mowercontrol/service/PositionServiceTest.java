package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.Orientation;
import fr.ippon.blablacar.mowercontrol.model.Position;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PositionServiceTest {

    @Test
    public void getNextPosition_shouldIncrementY_whenOrientationIsNorthAndGoForward() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.N);
        String command = "F";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(6);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.N);
    }

    @Test
    public void getNextPosition_shouldIncrementX_whenOrientationIsEastAndGoForward() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.E);
        String command = "F";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(6);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.E);
    }

    @Test
    public void getNextPosition_shouldDecrementY_whenOrientationIsSouthAndGoForward() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.S);
        String command = "F";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(4);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.S);
    }

    @Test
    public void getNextPosition_shouldDecrementX_whenOrientationIsWestAndGoForward() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.W);
        String command = "F";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(4);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.W);
    }

    @Test
    public void getNextPosition_shouldOrientToEast_whenOrientationIsNorthAndTurnRight() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.N);
        String command = "R";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.E);
    }

    @Test
    public void getNextPosition_shouldOrientToSouth_whenOrientationIsEastAndTurnRight() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.E);
        String command = "R";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.S);
    }

    @Test
    public void getNextPosition_shouldOrientToWest_whenOrientationIsSouthAndTurnRight() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.S);
        String command = "R";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.W);
    }

    @Test
    public void getNextPosition_shouldOrientToNorth_whenOrientationIsWestAndTurnRight() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.W);
        String command = "R";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.N);
    }

    @Test
    public void getNextPosition_shouldOrientToWest_whenOrientationIsNorthAndTurnLeft() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.N);
        String command = "L";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.W);
    }

    @Test
    public void getNextPosition_shouldOrientToNorth_whenOrientationIsEastAndTurnLeft() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.E);
        String command = "L";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.N);
    }

    @Test
    public void getNextPosition_shouldOrientToEast_whenOrientationIsSouthAndTurnLeft() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.S);
        String command = "L";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.E);
    }

    @Test
    public void getNextPosition_shouldOrientToSouth_whenOrientationIsWestAndTurnLeft() {
        // Given
        PositionService positionService = new PositionService();
        Position position = new Position(5, 5, Orientation.W);
        String command = "L";

        // When
        Position newPosition = positionService.applyMovementToPosition(command, position);

        // Then
        assertThat(newPosition.getX()).isEqualTo(5);
        assertThat(newPosition.getY()).isEqualTo(5);
        assertThat(newPosition.getOrientation()).isEqualTo(Orientation.S);
    }
}
