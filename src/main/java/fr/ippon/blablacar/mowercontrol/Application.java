package fr.ippon.blablacar.mowercontrol;

import fr.ippon.blablacar.mowercontrol.model.InitializationConfiguration;
import fr.ippon.blablacar.mowercontrol.service.CommanderService;
import fr.ippon.blablacar.mowercontrol.service.InitializationService;
import fr.ippon.blablacar.mowercontrol.service.LawnService;
import fr.ippon.blablacar.mowercontrol.service.MowerService;
import fr.ippon.blablacar.mowercontrol.service.PositionService;

public class Application {

    public static void main(String[] args) {
        InitializationService initializationService = new InitializationService();
        InitializationConfiguration initializationConfiguration = initializationService.extractConfiguration();

        MowerService mowerService = new MowerService(initializationConfiguration.getMowerList());
        PositionService positionService = new PositionService();
        LawnService lawnService = new LawnService(mowerService, positionService, initializationConfiguration.getLawn());

        CommanderService commanderService = new CommanderService(lawnService);

        lawnService.print();
        commanderService.process(initializationConfiguration.getMowerCommandsList());
        lawnService.print();
        mowerService.verbose();
    }

}
