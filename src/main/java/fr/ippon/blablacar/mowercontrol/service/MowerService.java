package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.Mower;

import java.util.Comparator;
import java.util.List;

public class MowerService {

    private final List<Mower> mowerList;

    public MowerService(List<Mower> mowerList) {
        this.mowerList = mowerList;
    }

    public Mower getById(Long id) {
        return this.mowerList.stream()
                .filter((currentMower) -> currentMower.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Mower> getAll() {
        return this.mowerList;
    }

    public void verbose() {
        this.mowerList.stream()
                .sorted(Comparator.comparingLong(Mower::getId))
                .forEach((mower) -> {
                    System.out.println(mower.getPosition().toString());
                });
    }

}
