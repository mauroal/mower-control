package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.InitializationConfiguration;
import fr.ippon.blablacar.mowercontrol.model.Lawn;
import fr.ippon.blablacar.mowercontrol.model.Mower;
import fr.ippon.blablacar.mowercontrol.model.MowerCommands;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class InitializationService {

    private static final String COMMAND_SEPARATOR = " ";

    public InitializationConfiguration extractConfiguration() {
        InitializationConfiguration initializationConfiguration = new InitializationConfiguration();

        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource("input.txt").toURI()))) {
            String lawnLine = bufferedReader.readLine();
            Lawn lawn = extractLawn(lawnLine);
            initializationConfiguration.setLawn(lawn);

            List<Mower> mowerList = new ArrayList<>();
            List<MowerCommands> mowerCommandsList = new ArrayList<>();

            long mowerIndexForIds = 1;
            String firstLine, secondLine;
            while (!isNullOrBlank(firstLine = bufferedReader.readLine()) && !isNullOrBlank(secondLine = bufferedReader.readLine())) {
                Mower mower = extractMower(firstLine, mowerIndexForIds);
                mowerList.add(mower);

                MowerCommands mowerCommands = new MowerCommands(mower.getId(), secondLine);
                mowerCommandsList.add(mowerCommands);

                mowerIndexForIds++;
            }
            initializationConfiguration.setMowerList(mowerList);
            initializationConfiguration.setMowerCommandsList(mowerCommandsList);
        } catch (IOException | URISyntaxException | RuntimeException e) {
            System.err.format("Error: %s", e.getMessage());
            System.exit(1);
        }

        return initializationConfiguration;
    }

    // VisibleForTesting
    protected Lawn extractLawn(String lawnLine) {
        String[] lawnSize;

        if (isNullOrBlank(lawnLine) || (lawnSize = lawnLine.split(COMMAND_SEPARATOR)).length < 2) {
            throw new RuntimeException("Lawn configuration should be like '5 5' at the first line of 'input.txt' file");
        }

        int width = Integer.parseInt(lawnSize[0]);
        int height = Integer.parseInt(lawnSize[1]);

        return new Lawn(width, height);
    }

    // VisibleForTesting
    protected Mower extractMower(String mowerLine, long mowerId) {
        String[] mowerInitialization;

        if (isNullOrBlank(mowerLine) || (mowerInitialization = mowerLine.split(COMMAND_SEPARATOR)).length < 3) {
            throw new RuntimeException("Mower configuration should be like '1 2 N'");
        }

        int x = Integer.parseInt(mowerInitialization[0]);
        int y = Integer.parseInt(mowerInitialization[1]);
        String orientation = mowerInitialization[2];

        return new Mower(mowerId, x, y, orientation);
    }

    private boolean isNullOrBlank(String s) {
        return s == null || s.isBlank();
    }

}
