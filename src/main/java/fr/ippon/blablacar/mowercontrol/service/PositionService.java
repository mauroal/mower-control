package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.Orientation;
import fr.ippon.blablacar.mowercontrol.model.Position;

public class PositionService {

    public Position applyMovementToPosition(String movement, Position position) {
        int x = position.getX();
        int y = position.getY();
        Orientation orientation = position.getOrientation();

        switch (movement) {
            case "L":
                switch (orientation) {
                    case N:
                        orientation = Orientation.W;
                        break;
                    case E:
                        orientation = Orientation.N;
                        break;
                    case S:
                        orientation = Orientation.E;
                        break;
                    case W:
                        orientation = Orientation.S;
                        break;
                }
                break;
            case "R":
                switch (orientation) {
                    case N:
                        orientation = Orientation.E;
                        break;
                    case E:
                        orientation = Orientation.S;
                        break;
                    case S:
                        orientation = Orientation.W;
                        break;
                    case W:
                        orientation = Orientation.N;
                        break;
                }
                break;
            case "F":
                switch (orientation) {
                    case N:
                        y++;
                        break;
                    case E:
                        x++;
                        break;
                    case S:
                        y--;
                        break;
                    case W:
                        x--;
                        break;
                }
                break;
        }

        return new Position(x, y, orientation);
    }

}
