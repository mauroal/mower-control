package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.Lawn;
import fr.ippon.blablacar.mowercontrol.model.Mower;
import fr.ippon.blablacar.mowercontrol.model.Orientation;
import fr.ippon.blablacar.mowercontrol.model.Position;

public class LawnService {

    private final Lawn lawn;

    private final MowerService mowerService;

    private final PositionService positionService;

    public LawnService(MowerService mowerService, PositionService positionService, Lawn lawn) {
        this.mowerService = mowerService;
        this.positionService = positionService;
        this.lawn = lawn;
    }

    public synchronized void moveMower(Long mowerId, String movement) {
        Mower mower = this.mowerService.getById(mowerId);
        Position nextPosition = positionService.applyMovementToPosition(movement, mower.getPosition());

        if (!movement.equals("F") || isValidPosition(nextPosition)) {
            mower.setPosition(nextPosition);
        }
    }

    private boolean isValidPosition(Position position) {
        return position.getX() >= 0
                && position.getX() <= lawn.getWidth()
                && position.getY() >= 0
                && position.getY() <= lawn.getHeight()
                && isFreePosition(position);
    }

    private boolean isFreePosition(Position position) {
        return this.mowerService.getAll()
                .stream()
                .map(Mower::getPosition)
                .allMatch((currentMowerPosition) -> position.getX() != currentMowerPosition.getX() || position.getY() != currentMowerPosition.getY());
    }

    public synchronized void print() {
        String lawnIterationSeparator = "###".repeat(lawn.getWidth() + 1);

        System.out.println(lawnIterationSeparator);
        mowerService.verbose();

        System.out.println(lawnIterationSeparator);
        for (int y = lawn.getHeight(); y >= 0; y--) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x <= lawn.getWidth(); x++) {
                final int X = x;
                final int Y = y;

                String c = mowerService.getAll().stream()
                        .filter((currentMower) -> currentMower.getPosition().getX() == X && currentMower.getPosition().getY() == Y)
                        .findFirst()
                        .map((currentMower) -> {
                            Orientation orientation = currentMower.getPosition().getOrientation();
                            switch (orientation) {
                                case N:
                                    return " A ";
                                case E:
                                    return " > ";
                                case S:
                                    return " V ";
                                case W:
                                    return " < ";
                                default:
                                    return " ? ";
                            }
                        })
                        .orElse(" * ");

                stringBuilder.append(c);
            }
            System.out.println(stringBuilder.toString());
        }
        System.out.println(lawnIterationSeparator);
    }

}
