package fr.ippon.blablacar.mowercontrol.service;

import fr.ippon.blablacar.mowercontrol.model.MowerCommands;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CommanderService {

    public LawnService lawnService;

    public CommanderService(LawnService lawnService) {
        this.lawnService = lawnService;
    }

    public void process(List<MowerCommands> mowerCommandsList) {
        ExecutorService executorService = Executors.newFixedThreadPool(mowerCommandsList.size());

        mowerCommandsList.forEach((currentMowerCommands) -> {
            executorService.submit(() -> {
                Long mowerId = currentMowerCommands.getMowerId();

                while (currentMowerCommands.getCommands().hasNext()) {
                    String movement = currentMowerCommands.getCommands().next();
                    this.lawnService.moveMower(mowerId, movement);
                }
            });
        });

        try {
            executorService.shutdown();
            executorService.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executorService.shutdownNow();
        }
    }

}
