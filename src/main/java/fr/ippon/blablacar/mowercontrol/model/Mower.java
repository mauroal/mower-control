package fr.ippon.blablacar.mowercontrol.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Mower {

    private Long id;

    private Position position;

    public Mower(Long id, int x, int y, String orientation) {
        this.id = id;
        this.position = new Position(x, y, orientation);
    }

}
