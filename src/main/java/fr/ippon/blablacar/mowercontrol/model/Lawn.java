package fr.ippon.blablacar.mowercontrol.model;

import lombok.Getter;

@Getter
public class Lawn {

    private final int width;

    private final int height;

    public Lawn(int width, int height) {
        this.width = width;
        this.height = height;
    }

}
