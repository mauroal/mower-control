package fr.ippon.blablacar.mowercontrol.model;

public enum Orientation {

    N,
    E,
    S,
    W

}
