package fr.ippon.blablacar.mowercontrol.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Position {

    private int x;

    private int y;

    private Orientation orientation;

    public Position(int x, int y, String orientation) {
        this(x, y, Orientation.valueOf(orientation));
    }

    public Position(int x, int y, Orientation orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public String toString() {
        return x + " " + y + " " + orientation.name();
    }
}
