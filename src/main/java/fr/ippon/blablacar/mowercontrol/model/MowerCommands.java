package fr.ippon.blablacar.mowercontrol.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Iterator;

@Getter
@Setter
public class MowerCommands {

    private Long mowerId;

    private Iterator<String> commands;

    public MowerCommands(Long mowerId, String commands) {
        this.mowerId = mowerId;
        this.commands = Arrays.stream(commands.split("")).iterator();
    }

}
