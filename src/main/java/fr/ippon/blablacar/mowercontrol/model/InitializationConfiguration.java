package fr.ippon.blablacar.mowercontrol.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class InitializationConfiguration {

    private Lawn lawn;

    private List<Mower> mowerList;

    private List<MowerCommands> mowerCommandsList;

}
